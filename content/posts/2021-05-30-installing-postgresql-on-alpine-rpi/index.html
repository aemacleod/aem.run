---
title: Installing PostgreSQL on Alpine RPi
date: '2021-05-30'
slug: 2021-05-30-installing-postgresql-on-alpine-rpi
---

<script src="{{< blogdown/postref >}}index_files/header-attrs/header-attrs.js"></script>


<p>Now that I have successfully <a href="https://aem.run/posts/setting-up-a-home-server/">installed Alpine</a> on the Raspberry Pi I will be using as my database server, it’s time to install PostgreSQL.</p>
<p>When doing something new and unfamiliar, it’s good practice to search and see if any guides exist. Since the work I’m doing doesn’t involve edge cases or exotic technologies, I usually find a guide that’s fairly close to what I’m doing.</p>
<div id="on-and-off-the-beaten-path" class="section level2">
<h2>On and off the beaten path</h2>
<p>That said, every field has its “ruts” and industry standard technologies, and the further you get from that beaten path the more manual work, intervention, and research you will need to do. To use my setup as an example:</p>
<ul>
<li>Alpine is a less common Linux distribution
<ul>
<li>Most documentation is written for more common server distributions like CentOS (Red Hat/IBM), Ubuntu (Canonical), and SUSE</li>
<li>Less common software packages might be outdated or not available in <code>apk</code>, the package manager for Alpine</li>
<li>Even if I choose to compile software from its source code manually, Alpine uses a non-standard compiler (<code>musl</code> instead of <code>glibc</code>) whose operation and quirks I would need to learn</li>
</ul></li>
<li>Raspberry Pi is nonstandard hardware for many server applications
<ul>
<li>It’s based on ARM architecture, which is still far behind X86 in terms of adoption and technical support for server products</li>
<li>It runs a closed source version of the <a href="https://en.wikipedia.org/wiki/ThreadX#Products_using_it">ThreadX</a> operating system, meaning Linux is actually a secondary operating system</li>
<li>Access to many low level hardware systems, notably temperature sensors, is not available directly to Linux. Even though the <a href="https://pkgs.alpinelinux.org/package/edge/main/aarch64/raspberrypi">Raspberry Pi tools</a> are supported on Alpine, learning how to get them working is another hoop</li>
</ul></li>
</ul>
<p>The level of technical difficulty increases the further one strays from the norm. If somebody theoretically wanted to use <a href="https://blogs.scientificamerican.com/roots-of-unity/dont-fall-for-babylonian-trigonometry-hype/">Babylonian numbers derived from Plimpton 322</a> for their trigonometric work, chances are good that they will have to put a significant amount of work into adjusting their workflow to support their use of the ancient digits.</p>
<p>Fortunately, my rig is not quite that uncommon. Another source of difficulty for a student like me comes from the combination of computer science being an intensely individual effort and the underappreciated challenges of emotional management in tech.</p>
</div>
<div id="google-searches-and-profanity" class="section level2">
<h2>Google searches and profanity</h2>
<p>Modern computing is obscenely complicated, and problems are constant: most things worth doing will regularly require venturing down rabbit holes into the minutiae in order to find solutions. This can be draining, frustrating, and sap motivation.</p>
<p>My brother once described Linux as being “powered by Google searches and profanity” and he’s right that the steep learning curve really elevates the importance of pacing and frustration management. Sometimes the pace of study is slowed down because something that “shouldn’t” be broken suddenly cuts off access to the main workflow.</p>
<p>I was recently very frustrated getting pgAdmin4 (a graphical interface program for postgres) to remotely connect to my server and went to sleep late upset and exhausted for several nights, trying to figure out why my connection was not working. Ultimately I found that the setting <code>PermitTunnel = no</code> in the file <code>/etc/ssh/sshd_config</code> was causing the Pi’s <code>ssh</code> server to block my laptop’s connections.</p>
<p>It might sound silly to grump and fuss about this, but lots of people give up on STEM careers and I expect that contributing factors include a lack of emotional management ability (which is in turn influenced by how and whether we talk about managing emotions) and venues where people can have experiences of community that help them to regulate themselves.</p>
<p>Finding out an optimal way of searching for solutions to endless tech problems is critical: it is draining to stay on one approach for too long and systematically exploit every possible solution that exists, but by jumping around too much there’s a risk of missing a solution or not understanding a dynamic that will come up again.</p>
<p>Sometimes disengaging and doing <em>anything</em> else and then coming back later, perhaps tomorrow, is the best approach.</p>
<p>I’ve found that with more experience I’ve developed a feeling for how much time I should spend going deep, how much time I should spend going wide, and when and how to try out different perspectives that change my approach to the problem.</p>
</div>
<div id="nothing-in-excess" class="section level2">
<h2>Nothing in excess</h2>
<p>Another challenge is tempering enjoyment. Even more often than they are frustrating, niche topics are a challenge for me because they are exhilirating: I love learning about technology.</p>
<p>I’m well in touch with my inner child, who would <a href="https://www.sciencedaily.com/releases/2020/08/200812153637.htm">rather explore than get rewards</a>. Understanding cryptography algorithms is a little beyond my current ability, but I enjoy reading about network security administration and the finer points of software like <code>ssh</code>.</p>
<p>I regularly go down 47-browser-tab rabbit holes studying the Linux kernel and I’ve made it most of the way through building <a href="https://www.linuxfromscratch.org/">Linux From Scratch</a>. I am fascinated by assembly code and want to learn it, including how penetration techniques like stack smashing work.</p>
<p>There are important topics, of course. <code>ssh</code> is a crucial technology and will be used for the foreseeable future, cybersecurity is not getting any less important, knowledge of the Linux kernel is useful every single day, and low level optimization, including assembly, is important in fields where every bit of performance matters.</p>
<p>Exhaustion is exhaustion, though. Your body doesn’t know whether sleep deprivation was caused by something that makes you happy. Exhaustion caused by joy is just as real as that caused by physical exertion and frustration, but happy exhaustion is more dangerous because it feels good and society eggs us on when we wear our selves out in the name of passion.</p>
<p>Whether it’s donkey-like frustration or the thrill of exploration causing me to be fixated on details of marginal importance, going too far off base eventually leaves me feeling muddled, lost, and unanchored from a sense of purpose.</p>
<p>I don’t have a perfect recipe for managing emotions, but consciously making an effort to improve continuously seems important. Basic Sesame Street interrogative techniques like “how am I feeling today? where is that feeling in my body?” are useful, and the field of <a href="https://aeon.co/essays/the-interoceptive-turn-is-maturing-as-a-rich-science-of-selfhood">interoception</a> informed by modern data-driven neuro research is generating lots of practical insights.</p>
<p>Fortunately for the rest of us, most data-driven researchers don’t have to bootstrap and administer their own servers, but my database isn’t going to install itself.</p>
</div>
<div id="installing-postgres" class="section level2">
<h2>Installing postgres</h2>
<p>A note on security: this guide is not concerned with securing a database beyond basic password protection. I am working on a home network and not exposing my database directly to the open internet.</p>
<p>My goal here is to learn how to run SQL queries, not to take a deep dive into how security standards and obscure configuration files interact with each other. When connecting programs with each other, tighter security settings cause cryptic errors and resolving them drains precious time and causes frustration.</p>
<p>There is always some risk, particularly from any compromised device on your network, but this is not a system we plan to keep around for a long time or deploy in critical or sensitive roles, so the risk is tolerable. Don’t do this over public wifi or on any untrusted network.</p>
<p>This guide is largely built from <a href="https://luppeng.wordpress.com/2020/02/28/install-and-start-postgresql-on-alpine-linux/">this well-written one</a>. The primary difference is that I am explicitly focusing on a diskless install and using <code>lbu</code> to commit changes to the apk overlay so that they persist between boots.</p>
<ol style="list-style-type: decimal">
<li><p>Update system and add nano for easier config file editing, unless you like to use <code>vi</code>:<br />
<code>apk update</code><br />
<code>apk upgrade</code><br />
<code>apk add nano</code></p></li>
<li><p>Create postgres user and give them admin privileges:<br />
<code>adduser -h /var/lib/postgresql/ postgres</code><br />
<code>EDITOR=nano visudo</code><br />
In the config file, add postgres to the sudoers section underneath <code>root</code>:</p>
<pre><code> ## User privilege specification  
 ##  
 root ALL=(ALL) ALL  
 postgres ALL=(ALL) ALL</code></pre></li>
<li><p>Switch to the postgres account, install postgres, and create the connection socket folder:<br />
<code>su - postgres</code><br />
<code>apk add postgresql</code><br />
<code>mkdir /run/postgresql/</code><br />
<code>chown postgres:postgres /run/postgresql/</code></p></li>
<li><p>Add the sockets folder to <code>lbu</code> to persist it, because Alpine only persists <code>/etc</code> between reboots by default:<br />
<code>lbu add /run/postgresql/</code></p></li>
<li><p>Do the same for the data directory where the database files will be located and only let user postgres modify the directory:<br />
<code>mkdir /var/lib/postgresql/data/</code><br />
<code>chown postgres:postgres /var/lib/postgresql/data/</code><br />
<code>touch var/lib/postgresql/.psql_history</code><br />
<code>chown postgres:postgres /var/lib/postgresql/data/.psql_history</code><br />
<code>chmod 0700 /var/lib/postgresql/data/</code><br />
<code>lbu add /var/lib/postgresql/data/</code><br />
<code>lbu add /var/lib/postgresql/data/.psql_history</code><br />
</p></li>
<li><p><strong>IMPORTANT</strong>: Run the <code>lbu commit -d</code> command to commit the changes we made to the Pi’s SD card. The <code>-d</code> flag deletes old overlay files so that they don’t hang around and junk up our SD card. It’s good practice to get in the habit of running <code>lbu commit -d</code> regularly so that we don’t lose work.<br />
We are running Alpine from RAM (aka diskless), which means changes are not automatically saved to the SD card the way they are in most operating systems. This <strong>includes</strong> the information in our database. I should probably automate periodic commits with a shell script at some point.</p></li>
<li><p>Initialize the database with <code>initdb -D /var/lib/postgresql/data</code>. Some of the time zones options in the Alpine <code>setup-timezone</code> script cause errors with postgres:</p>
<pre><code> running bootstrap script ... 2021-05-28 15:42:07.769 GMT [3553] LOG:  invalid value for parameter &quot;log_timezone&quot;: &quot;US//Eastern&quot;  
 2021-05-28 15:42:07.770 GMT [3553] LOG:  invalid value for parameter &quot;TimeZone&quot;: &quot;US//Eastern&quot;</code></pre>
<p>In my case, US//Eastern was not recognized, so I ran <code>setup-timezone</code> again and entered <code>EST5EDT</code> (Eastern Standard Time -5 GMT Daylight Savings Time) and the command to initialize ran successfully.</p></li>
<li><p>To enable remote connections, which we plan to do from RStudio and pgAdmin4, type <code>echo "host all all 0.0.0.0/0 md5" &gt;&gt; /var/lib/postgresql/data/pg_hba.conf</code> to modify the client config file. Then enter <code>nano /var/lib/postgresql/data/postgresql.conf</code> to edit the postgres config file. Uncomment and modify the following lines as such:</p>
<ul>
<li><code>listen_addresses = '*'</code></li>
<li><code>port = 5432</code></li>
</ul></li>
<li><p>Start the server with <code>pg_ctl start -D /var/lib/postgresql/data/</code>. This may work without issue, but I got the following error:</p>
<pre><code> localhost:/var/lib/postgresql$ pg_ctl start -D /var/lib/postgresql/data
 waiting for server to start....2021-05-28 14:18:11.231 EDT [2894] LOG:  starting PostgreSQL 13.3 on aarch64-alpine-linux-musl, compiled by gcc (Alpine 10.2.1_pre1) 10.2.1 20201203, 64-bit
 2021-05-28 14:18:11.231 EDT [2894] LOG:  could not bind IPv4 address &quot;0.0.0.0&quot;: Address in use
 2021-05-28 14:18:11.231 EDT [2894] HINT:  Is another postmaster already running on port 5432? If not, wait a few seconds and retry.
 2021-05-28 14:18:11.231 EDT [2894] WARNING:  could not create listen socket for &quot;0.0.0.0&quot;
 2021-05-28 14:18:11.231 EDT [2894] FATAL:  could not create any TCP/IP sockets
 2021-05-28 14:18:11.245 EDT [2894] LOG:  database system is shut down
 stopped waiting
 pg_ctl: could not start server
 Examine the log output.</code></pre>
<p>Port 5432 was being used by an instance of postgres that I started while writing and testing these instructions that didn’t shut down properly, so I ran <code>sudo netstat -tulpn | grep 5432</code> to identify the process using port 5432 (in this instance process 2660) and then typed <code>kill 2660</code>. Starting the database worked normally after that.</p></li>
<li><p>Test the database:<br />
<code>psql</code> Confirm you can log in<br />
<code>\c</code> Confirm that account postgres works<br />
<code>SELECT datname FROM pg_database;</code> Confirm database exists<br />
<code>CREATE DATABASE demo;</code> Test database creation<br />
<code>SELECT datname FROM pg_database;</code> Confirm test database was created<br />
</p></li>
<li><p>Add an executable script to start postgres when the system boots:<br />
<code>touch /etc/local.d/postgres-custom.start</code><br />
<code>chmod +x /etc/local.d/postgres-custom.start</code><br />
<code>nano /etc/local.d/postgres-custom.start</code><br />
Then we’ll add this script to the file:</p>
<pre><code> #!/bin/sh
 su postgres -c &#39;pg_ctl start -D /var/lib/postgresql/data&#39;</code></pre></li>
<li><p>Commit our changes so that they persist, and we’ll reboot the system to test that everything is working correctly.<br />
<code>lbu commit -d</code> Note that commit will take longer the more data it’s writing<br />
<code>reboot</code><br />
Confirm that the demo database was persisted from the postgres account with the commands <code>psql</code> and <code>SELECT datname FROM pg_database;</code>. If both commands work as expected, then the database is booting and persisting as it should. Many errors can happen that prevent this, however.</p></li>
</ol>
</div>
<div id="common-issues" class="section level2">
<h2>Common issues</h2>
<ol style="list-style-type: decimal">
<li><p>I haven’t figured out why yet, but some Alpine installs fail to persist the socket directory <code>/run/postgresql/</code> between reboots. I powered down the Pi and looked at the SD card on my computer and <code>/run/postgresql/</code> had been properly saved in the apk file, so <code>lbu add</code> and <code>lbu commit -d</code> are doing what they are supposed to do.<br />
Something is happening in the operating system to cause <code>/run/postgresql/</code> but not <code>/var/lib/postgresql/</code> to fail to load in RAM. I have had installs where this issue occurs and installs where it does not.<br />
If postgres expects the socket folder to exist at runtime and it does not, then the database will fail to load. When you reboot, if <code>/run/postgresql</code> doesn’t exist, then open <code>/var/lib/postgresql/data/postgresql.conf</code> and remove it from the socket directories setting:<br />
<code>unix_socket_directories = '/tmp'</code><br />
</p></li>
<li><p>Starting up the database manually gives the following error:</p>
<pre><code> localhost:/var/lib/postgresql$ pg_ctl start -D /var/lib/postgresql/data/
 pg_ctl: could not open PID file &quot;/var/lib/postgresql/data/postmaster.pid&quot;: Permission denied</code></pre></li>
</ol>
<p>This happens because the file owner gets changed from postgres to some numerical value between reboots, preventing postgres from starting the database. Change it back manually with <code>chown postgres:postgres /var/lib/postgresql/ -R</code>. It’s a good idea to also add that command to our startup script located at <code>/etc/local.d/postgres-custom.start</code> so that it now reads as follows:</p>
<pre><code>    #!/bin/sh
    chown postgres:postgres /var/lib/postgresql/ -R
    su postgres -c &#39;pg_ctl start -D /var/lib/postgresql/data&#39;</code></pre>
</div>
<div id="conclusion" class="section level2">
<h2>Conclusion</h2>
<p>Now we have an empty PostgreSQL database running on the Alpine Pi. Managing persistence with a diskless install is a bit of a headache, but some basic error parsing, scripting knowledge, and Google Fu should be enough to troubleshoot the headaches that pop up. Once the database is running it shouldn’t need to reboot very often anyway, so hopefully these issues won’t be common.</p>
<p>Our next steps are to connect our database with pgAdmin4, the postgres-specific GUI application. Other well-regarded programs also exist, particularly DBeaver, but I am going to focus on learning pgAdmin first. After that, we will connect the database to RStudio and begin working with queries and basic data manipulation.</p>
</div>
