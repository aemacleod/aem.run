---
title: Beautifying a table
date: '2021-06-10'
slug: 2021-06-10-beautifying-a-table
output:
  html_document:
    df_print: kable
---

<script src="{{< blogdown/postref >}}index_files/header-attrs/header-attrs.js"></script>
<script src="{{< blogdown/postref >}}index_files/kePrint/kePrint.js"></script>
<link href="{{< blogdown/postref >}}index_files/lightable/lightable.css" rel="stylesheet" />


<p>After successfully <a href="https://aem.run/posts/2021-06-06-running-sql-queries-in-rstudio/">querying my pi-gres database</a> with SQL commands, retrieving data, and saving it locally, it’s time to make the data presentable. This is easier said than done: while it’s possible to scrape something together that works well enough, I have several other considerations:</p>
<ul>
<li>Portability and reusability: as much fun as I have taking deep dives into the documentation (and I only say that half sarcastically), I want to write code that I can use in other projects without substantially rebuilding it every time.</li>
<li>Compatibility: my primary concern is blogging for now, so the ability to demonstrate code and style the appearance of outputs is important. R Markdown and associated packages are quite capable, but I also eventually want to create <code>htmlwidgets</code>, web apps with <code>shiny</code>, and maybe dashboards.</li>
<li>Readability and best practices: while I don’t have any reservations about going against the grain, especially when cobbling a project together at the start, best practices exist for a reason and it’s always good to consider the poor people who will have to read, or God forbid maintain, this code.</li>
</ul>
<p>I also want to be mindful of the balance between overthinking and flailing aimlessly in the name of “just doing it.” With that in mind, this is how I am proceeding.</p>
<div id="setup-and-r-markdown" class="section level2">
<h2>Setup and R Markdown</h2>
<p>I’m new to R Markdown and still learning its ins and outs, so I’ll be leaning a lot on two courses that were very helpful for me: <a href="https://www.linkedin.com/learning/creating-reports-and-presentations-with-r-markdown-and-rstudio/">Creating Reports and Presentations with R Markdown and RStudio</a> taught by Charlie Joey Hadley, and <a href="https://www.linkedin.com/learning/data-wrangling-in-r/">Data Wrangling in R</a> taught by Mike Chapple.</p>
<p>As an aside, LinkedIn Learning is <a href="https://www.nypl.org/collections/articles-databases/lyndacom">available for free</a> through the New York Public Library. I have used their catalog heavily in recent years and recommend them if you don’t already have a training site (Coursera, Khan Academy, Udemy, edX, etc.) that you like.</p>
<p>As I work here, I have several goals. Specifically, I want to:</p>
<ul>
<li>Accurately manipulate the table to make it appear how I want</li>
<li>Use <code>knit</code>, <code>kable</code>, and <code>kableExtra</code> packages to make it look pretty and play well with my blog’s theme</li>
<li>Control the appearance of live code chunks and make sure I don’t break things</li>
</ul>
<p>R Markdown documents are different from most Markdown in that code can be run live and the results displayed. By contrast, regular code blocks made with ``` don’t do anything:</p>
<pre><code>penguin@linux:~$ sudo dd if=/dev/urandom of=/dev/nvme0n1 bs=1M</code></pre>
<p>The above code is inside a code block and doesn’t do anything, which is fortunate because it is a command to write over my hard drive with random data. A live code block <em>looks</em> like the one below when written in code, but I’ve escaped it with <a href="https://bookdown.org/yihui/rmarkdown-cookbook/verbatim-code-chunks.html">this technique</a> so it’s not live:</p>
<pre><code>```{r setup}
knitr::opts_chunk$set(
    echo = TRUE,
    message = FALSE,
    warning = FALSE
)
```</code></pre>
<p>Now here the same chunk is again, but this time it’s live:</p>
<pre class="r"><code>knitr::opts_chunk$set(
    echo = TRUE,
    message = FALSE,
    warning = FALSE
)</code></pre>
<p>In this case, the code is setting the defaults for messages generated when code is run. The point of a blog is presentation, and hopefully for my audience’s sake I’m not presenting a bunch of junk code that throws errors, as console output is visually distracting, very probably unrelated to the message, and something most readers generally don’t want.</p>
<p>Then again, I do like to take pictures of the Blue Screen of Death whenever I see it in unexpected places, like a subway train arrival time board. The next time I have to make a really boring presentation, maybe I’ll lighten the mood by including a kernel panic.</p>
<p>In an actual presentation, I’ll hide a code block like this with the <code>include=FALSE</code> flag so that the code is run, but it isn’t shown in the final document. This is a post to demonstrate, though, so code blocks make some sense here. Next, we’ll load the table from my local drive and display it with the default settings.</p>
<pre class="r"><code>library(tidyverse)
data &lt;- read_csv(&quot;table.csv&quot;)
print(data)</code></pre>
<pre><code>## # A tibble: 8 x 5
##   dataset_title        num_columns num_rows notes              url              
##   &lt;chr&gt;                      &lt;dbl&gt;    &lt;dbl&gt; &lt;chr&gt;              &lt;chr&gt;            
## 1 Drinking Water Qual…          11    95400 Many sites (~400)… https://data.cit…
## 2 Distribution Sites …          10     1411 Many testing site… https://data.cit…
## 3 Entry Point LCR Mon…          10     8181 Testing sites are… https://data.cit…
## 4 Watershed Water Qua…          10     2431 Simple data set c… https://data.cit…
## 5 DEP - Cryptosporidi…           5     1065 Sample sites are … https://data.cit…
## 6 Compliance at-the-t…           7     2871 Compelling data s… https://data.cit…
## 7 Harbor Water Quality         100    92000 Many sites (40+),… https://data.cit…
## 8 Current Reservoir L…          25     1278 Rows are reservoi… https://data.cit…</code></pre>
<p>Golly, that’s ugly. I really like the IBM Plex Mono typeface, but the table had nothing to do with that. It needs a little work before it’s fit for human consumption.</p>
</div>
<div id="very-basic-data-wrangling-and-table-formatting" class="section level2">
<h2><em>Very</em> basic data wrangling and table formatting</h2>
<p>When I first learned about data wrangling in R, I thought it would be fun to look at the <a href="https://data.cityofnewyork.us/City-Government/Parking-Violations-Issued-Fiscal-Year-2019/faiq-9dfq">NYC Parking Violations dataset</a>. Then I ran the <code>unique()</code> command on the vehicle color column and discovered that the fine people of the NYC Transportation Bureau had listed 1,830 different vehicle colors.</p>
<p>My favorite entry was “B.” Blue? Black? Beige? Brown? Burple? ¯\_(ツ)_/¯ Long story short, I’m not ready to tackle that dataset yet. I look forward to doing so, but it’s a stretch goal. The reason I created this example table was to list some data sets that seemed reasonably easy to manage and progressively build my skills against.</p>
<p>The challenge with data wrangling, as with much of programming, is that ideas that are simple for a human to express are difficult to translate into code. In this case, these are the <strong>very</strong> basic manipulations I want to do to this table:</p>
<ul>
<li>Turn the dataset title into a hyperlink</li>
<li>Clean up the column names</li>
<li>Get rid of the URL column once it’s no longer needed</li>
</ul>
<p>Fortunately, I’ve had some experience with string manipulation dating back to my <a href="https://github.com/aemacleod/eight-frame">very first coding project</a> (RIP Pebble) so the topic wasn’t completely new. Essentially, the instructions above turned into this:</p>
<ul>
<li>Add brackets [] before and after the dataset title and add parentheses () before and after the URL</li>
<li>Crate a new column using these modified columns and call it Dataset (voila! Markdown hyperlink)</li>
<li>Recreate the table with Dataset at the front, rename Columns, Rows, and Notes, drop dataset_title and url</li>
</ul>
<p>Again, this is super simple and even this required a bit of diving around the documentation. In any language as old as R, there are multiple ways to do simple operations (e.g. <code>paste()</code>), there are deprecated packages (I encountered <code>write_csv()</code> recently), and then there are best practices to consider (like consistency in using the <code>tidyverse</code>).</p>
<p>After spinning those wheels for a while I had to stop myself from overthinking and just write. I’m not going to get it perfect, but perfect is the enemy of finishing anything. This is what I did:</p>
<pre class="r"><code>data &lt;- data %&gt;%
  mutate(Dataset = str_c(&quot;[&quot;,data$dataset_title,&quot;]&quot;,
                         &quot;(&quot;,data$url,&quot;)&quot;)
         ) %&gt;%
  select(Dataset, Columns = num_columns,
         Rows = num_rows, Notes = notes,
         -dataset_title, -url) %&gt;%
  print()</code></pre>
<pre><code>## # A tibble: 8 x 4
##   Dataset                           Columns  Rows Notes                         
##   &lt;chr&gt;                               &lt;dbl&gt; &lt;dbl&gt; &lt;chr&gt;                         
## 1 [Drinking Water Quality Distribu…      11 95400 Many sites (~400), fewer vari…
## 2 [Distribution Sites LCR Monitori…      10  1411 Many testing sites, compellin…
## 3 [Entry Point LCR Monitoring Resu…      10  8181 Testing sites are rows, would…
## 4 [Watershed Water Quality Data](h…      10  2431 Simple data set conceptually …
## 5 [DEP - Cryptosporidium And Giard…       5  1065 Sample sites are listed in ro…
## 6 [Compliance at-the-tap Lead and …       7  2871 Compelling data set, has pote…
## 7 [Harbor Water Quality](https://d…     100 92000 Many sites (40+), rows, and o…
## 8 [Current Reservoir Levels](https…      25  1278 Rows are reservoirs, columns …</code></pre>
<p>Now it’s even less legible, but the Dataset column is now capable of hyperlinking with Markdown and the column headers are human-friendly. I’ll turn it into a cleaner table. There are a lot of R packages for working with tables. A whole lot.</p>
<p><code>condformat</code>, <code>DT</code>, <code>flextable</code>, <code>formattable</code>, <code>ftextra</code>, <code>gt</code>, <code>huxtable</code>, <code>kableExtra</code>, <code>knitr</code>, <code>pander</code>, <code>pixiedust</code>, <code>reactable</code>, <code>rhandsontable</code>, <code>stargazer</code>, <code>tables</code>, <code>tangram</code>, <code>xtable</code>, <code>ztable</code>.</p>
<p>At some point I’d like to experiment with the different packages, but for now I’ll start with <code>knitr::kable()</code> because it’s part of the <code>tidyverse</code>, which in my limited experience is a cohesively designed set of packages with a clean API and good documentation. Being easy for beginners is more important for me right now than being the “best” technical offering.</p>
<p>Here’s the table with <code>kable()</code>:</p>
<pre class="r"><code>data %&gt;% knitr::kable()</code></pre>
<table>
<colgroup>
<col width="37%" />
<col width="1%" />
<col width="1%" />
<col width="58%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Dataset</th>
<th align="right">Columns</th>
<th align="right">Rows</th>
<th align="left">Notes</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Drinking-Water-Quality-Distribution-Monitoring-Dat/bkwf-xfky">Drinking Water Quality Distribution Monitoring Data</a></td>
<td align="right">11</td>
<td align="right">95400</td>
<td align="left">Many sites (~400), fewer variables, many observations, would be good set for data wrangling</td>
</tr>
<tr class="even">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Distribution-Sites-LCR-Monitoring-Results/g63j-swsd">Distribution Sites LCR Monitoring Results</a></td>
<td align="right">10</td>
<td align="right">1411</td>
<td align="left">Many testing sites, compelling data (lead and copper at the tap), relatively simple set would be good for more impactful analysis (e.g. statistical approaches, storytelling)</td>
</tr>
<tr class="odd">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Entry-Point-LCR-Monitoring-Results/aa5e-digs">Entry Point LCR Monitoring Results</a></td>
<td align="right">10</td>
<td align="right">8181</td>
<td align="left">Testing sites are rows, would be good tidy data presentation, is a fairly simple data set</td>
</tr>
<tr class="even">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Watershed-Water-Quality-Data/y43c-5n92">Watershed Water Quality Data</a></td>
<td align="right">10</td>
<td align="right">2431</td>
<td align="left">Simple data set conceptually and structurally, could be used for more substantial analyses, e.g. stats, multivariate, regressions</td>
</tr>
<tr class="odd">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/DEP-Cryptosporidium-And-Giardia-Data-Set/x2s6-6d2j">DEP - Cryptosporidium And Giardia Data Set</a></td>
<td align="right">5</td>
<td align="right">1065</td>
<td align="left">Sample sites are listed in rows rather than as column variables, this would be good tidy data presentation. Data overlaps with Watershed Water Quality Data, tables could be pulled together for more complex SQL analysis or data table manipulations</td>
</tr>
<tr class="even">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Compliance-at-the-tap-Lead-and-Copper-Data/3wxk-qa8q">Compliance at-the-tap Lead and Copper Data</a></td>
<td align="right">7</td>
<td align="right">2871</td>
<td align="left">Compelling data set, has potential interest or applicability for municipal nerds and civil groups</td>
</tr>
<tr class="odd">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Harbor-Water-Quality/5uug-f49n">Harbor Water Quality</a></td>
<td align="right">100</td>
<td align="right">92000</td>
<td align="left">Many sites (40+), rows, and observation, would be good exercise for data wrangling and visualization</td>
</tr>
<tr class="even">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Current-Reservoir-Levels/zkky-n5j3">Current Reservoir Levels</a></td>
<td align="right">25</td>
<td align="right">1278</td>
<td align="left">Rows are reservoirs, columns are measurements, would make a good tidy data presentation</td>
</tr>
</tbody>
</table>
<p>It looks much better, and honestly I’d be comfortable publishing this. I know standard practice is to right-justify numbers, but that visually makes the most sense when there are many rows of numbers and they’re bound by a grid. When there are only a few numbers and lots of white space, I think centering numbers is more sensible. Let’s also add a caption.</p>
<pre class="r"><code>data %&gt;%
  knitr::kable(align=&#39;lccl&#39;,
               caption=&#39;NYC Environmental Open Data Sets of Interest&#39;)</code></pre>
<table>
<caption><span id="tab:kable-options">Table 1: </span>NYC Environmental Open Data Sets of Interest</caption>
<colgroup>
<col width="37%" />
<col width="2%" />
<col width="1%" />
<col width="58%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Dataset</th>
<th align="center">Columns</th>
<th align="center">Rows</th>
<th align="left">Notes</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Drinking-Water-Quality-Distribution-Monitoring-Dat/bkwf-xfky">Drinking Water Quality Distribution Monitoring Data</a></td>
<td align="center">11</td>
<td align="center">95400</td>
<td align="left">Many sites (~400), fewer variables, many observations, would be good set for data wrangling</td>
</tr>
<tr class="even">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Distribution-Sites-LCR-Monitoring-Results/g63j-swsd">Distribution Sites LCR Monitoring Results</a></td>
<td align="center">10</td>
<td align="center">1411</td>
<td align="left">Many testing sites, compelling data (lead and copper at the tap), relatively simple set would be good for more impactful analysis (e.g. statistical approaches, storytelling)</td>
</tr>
<tr class="odd">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Entry-Point-LCR-Monitoring-Results/aa5e-digs">Entry Point LCR Monitoring Results</a></td>
<td align="center">10</td>
<td align="center">8181</td>
<td align="left">Testing sites are rows, would be good tidy data presentation, is a fairly simple data set</td>
</tr>
<tr class="even">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Watershed-Water-Quality-Data/y43c-5n92">Watershed Water Quality Data</a></td>
<td align="center">10</td>
<td align="center">2431</td>
<td align="left">Simple data set conceptually and structurally, could be used for more substantial analyses, e.g. stats, multivariate, regressions</td>
</tr>
<tr class="odd">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/DEP-Cryptosporidium-And-Giardia-Data-Set/x2s6-6d2j">DEP - Cryptosporidium And Giardia Data Set</a></td>
<td align="center">5</td>
<td align="center">1065</td>
<td align="left">Sample sites are listed in rows rather than as column variables, this would be good tidy data presentation. Data overlaps with Watershed Water Quality Data, tables could be pulled together for more complex SQL analysis or data table manipulations</td>
</tr>
<tr class="even">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Compliance-at-the-tap-Lead-and-Copper-Data/3wxk-qa8q">Compliance at-the-tap Lead and Copper Data</a></td>
<td align="center">7</td>
<td align="center">2871</td>
<td align="left">Compelling data set, has potential interest or applicability for municipal nerds and civil groups</td>
</tr>
<tr class="odd">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Harbor-Water-Quality/5uug-f49n">Harbor Water Quality</a></td>
<td align="center">100</td>
<td align="center">92000</td>
<td align="left">Many sites (40+), rows, and observation, would be good exercise for data wrangling and visualization</td>
</tr>
<tr class="even">
<td align="left"><a href="https://data.cityofnewyork.us/Environment/Current-Reservoir-Levels/zkky-n5j3">Current Reservoir Levels</a></td>
<td align="center">25</td>
<td align="center">1278</td>
<td align="left">Rows are reservoirs, columns are measurements, would make a good tidy data presentation</td>
</tr>
</tbody>
</table>
<p>I’m not a fan of the forced “Table 1” prefix, and I’d like to give the Notes tab some more breathing space, but the <a href="https://www.rdocumentation.org/packages/knitr/versions/1.33/topics/kable">kable documentation</a> shows me that there really isn’t a lot of ability to modify <code>kable</code> tables. To do a little more, I’ll use the <code>kableExtra</code> package.</p>
<pre class="r"><code>library(kableExtra)
data %&gt;%
  kbl(align=&#39;lccl&#39;) %&gt;%
  kable_styling(font_size=12)</code></pre>
<table class="table" style="font-size: 12px; margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Dataset
</th>
<th style="text-align:center;">
Columns
</th>
<th style="text-align:center;">
Rows
</th>
<th style="text-align:left;">
Notes
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;">
<a href="https://data.cityofnewyork.us/Environment/Drinking-Water-Quality-Distribution-Monitoring-Dat/bkwf-xfky">Drinking Water Quality Distribution Monitoring Data</a>
</td>
<td style="text-align:center;">
11
</td>
<td style="text-align:center;">
95400
</td>
<td style="text-align:left;">
Many sites (~400), fewer variables, many observations, would be good set for data wrangling
</td>
</tr>
<tr>
<td style="text-align:left;">
<a href="https://data.cityofnewyork.us/Environment/Distribution-Sites-LCR-Monitoring-Results/g63j-swsd">Distribution Sites LCR Monitoring Results</a>
</td>
<td style="text-align:center;">
10
</td>
<td style="text-align:center;">
1411
</td>
<td style="text-align:left;">
Many testing sites, compelling data (lead and copper at the tap), relatively simple set would be good for more impactful analysis (e.g. statistical approaches, storytelling)
</td>
</tr>
<tr>
<td style="text-align:left;">
<a href="https://data.cityofnewyork.us/Environment/Entry-Point-LCR-Monitoring-Results/aa5e-digs">Entry Point LCR Monitoring Results</a>
</td>
<td style="text-align:center;">
10
</td>
<td style="text-align:center;">
8181
</td>
<td style="text-align:left;">
Testing sites are rows, would be good tidy data presentation, is a fairly simple data set
</td>
</tr>
<tr>
<td style="text-align:left;">
<a href="https://data.cityofnewyork.us/Environment/Watershed-Water-Quality-Data/y43c-5n92">Watershed Water Quality Data</a>
</td>
<td style="text-align:center;">
10
</td>
<td style="text-align:center;">
2431
</td>
<td style="text-align:left;">
Simple data set conceptually and structurally, could be used for more substantial analyses, e.g. stats, multivariate, regressions
</td>
</tr>
<tr>
<td style="text-align:left;">
<a href="https://data.cityofnewyork.us/Environment/DEP-Cryptosporidium-And-Giardia-Data-Set/x2s6-6d2j">DEP - Cryptosporidium And Giardia Data Set</a>
</td>
<td style="text-align:center;">
5
</td>
<td style="text-align:center;">
1065
</td>
<td style="text-align:left;">
Sample sites are listed in rows rather than as column variables, this would be good tidy data presentation. Data overlaps with Watershed Water Quality Data, tables could be pulled together for more complex SQL analysis or data table manipulations
</td>
</tr>
<tr>
<td style="text-align:left;">
<a href="https://data.cityofnewyork.us/Environment/Compliance-at-the-tap-Lead-and-Copper-Data/3wxk-qa8q">Compliance at-the-tap Lead and Copper Data</a>
</td>
<td style="text-align:center;">
7
</td>
<td style="text-align:center;">
2871
</td>
<td style="text-align:left;">
Compelling data set, has potential interest or applicability for municipal nerds and civil groups
</td>
</tr>
<tr>
<td style="text-align:left;">
<a href="https://data.cityofnewyork.us/Environment/Harbor-Water-Quality/5uug-f49n">Harbor Water Quality</a>
</td>
<td style="text-align:center;">
100
</td>
<td style="text-align:center;">
92000
</td>
<td style="text-align:left;">
Many sites (40+), rows, and observation, would be good exercise for data wrangling and visualization
</td>
</tr>
<tr>
<td style="text-align:left;">
<a href="https://data.cityofnewyork.us/Environment/Current-Reservoir-Levels/zkky-n5j3">Current Reservoir Levels</a>
</td>
<td style="text-align:center;">
25
</td>
<td style="text-align:center;">
1278
</td>
<td style="text-align:left;">
Rows are reservoirs, columns are measurements, would make a good tidy data presentation
</td>
</tr>
</tbody>
</table>
<p>After experimenting with the package a bit, I can see a lot of its options butt heads with my blog’s theme, presumably the blog’s CSS styling overrides what kableExtra wants to do. I’m okay with that. For now, the table is reasonably sized and styled in a way I’m comfortable with.</p>
<p>I want to think about formatting and style, but I’d rather wait until the thing I am polishing is not a turd. Now that I have the table in a presentable format, I think it’s time to wrap this project up and start working on something else.</p>
</div>
