---
title: Creating a ggplot2 theme
date: '2021-07-16'
slug: [2021-07-16-creating-a-ggplot2-theme]
---

<script src="{{< blogdown/postref >}}index_files/header-attrs/header-attrs.js"></script>


<p>As I learn more about exploratory data analysis, I’ve started working with R’s visualization libraries and learning topics like <a href="https://aem.run/posts/2021-07-11-managing-inconsistent-facets-with-patchwork/">faceting</a>, <a href="https://aem.run/posts/2021-07-02-learning-the-basics-of-gis-mapping-with-leaflet/">mapping</a>, and basic <a href="https://aem.run/posts/2021-06-29-exploring-visualization-libraries/">interactive tables</a>.</p>
<p>As I work with ggplot more, I find myself tweaking the graphics I’ve built quite a bit. In building manual <code>patchwork</code> graphics, this created a lot of repetitive code. Much of this was related to the quirks of the data and was partly unavoidable given my approach, but a lot of duplicated code was produced to manage the theme elements of the plots.</p>
<div id="elements-of-a-ggplot-theme" class="section level2">
<h2>Elements of a ggplot theme</h2>
<p>It’s easier to show ggplot theming than to talk about it, however, so let’s look at a facet grid I wrote previously, but this time stripped of theme updates so that only the default <code>theme_gray()</code> is used. Then we will modify the major elements that allow us to change many individual minor elements all at once.</p>
<p>If you are unfamiliar with how ggplot code structures a graphic, we start by piping our data (creatively called <code>data</code>) using the pipe operator <code>%&gt;%</code> into an empty plot (<code>ggplot()</code>) and add elements using the <code>+</code> operator, in this case a boxplot with <code>geom_boxplot()</code>.</p>
<pre class="r"><code>data %&gt;%
  ggplot() +
  geom_boxplot(
    mapping = aes(
      x = Reservoir,
      y = Release,
      fill = Reservoir)
    ) + 
  facet_wrap(
    Site ~ .,
    scales = &quot;free_y&quot;) +
  labs(
    title = &quot;Water Release from Upstate Reservoirs (millions of gallons)&quot;,
    caption = &quot;Source: NYC Department of Environmental Protection&quot;
  )</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/release_facet_wrap-1.svg" width="672" /></p>
<p>I find the default theme very ugly, but it’s sufficient to demonstrate each major element of a ggplot theme: lines, rectangles, text and title, and aspect ratio. When these major elements are modified, many minor elements of the theme will inherit the changes. Minor element categories include the axes, legend, panel, plot, and strips.</p>
<p>We’ll use <code>?theme()</code> to read the docs and identify specific elements to modify.</p>
<pre class="r"><code>data %&gt;%
  ggplot() +
  geom_boxplot(
    mapping = aes(
      x = Reservoir,
      y = Release,
      fill = Reservoir)
    ) + 
  facet_wrap(
    Site ~ .,
    scales = &quot;free_y&quot;) +
  labs(
    title = &quot;Water Release from Upstate Reservoirs (millions of gallons)&quot;,
    caption = &quot;Source: NYC Department of Environmental Protection&quot;
  ) +
  theme(
    rect = element_rect(fill = &quot;#55aaff&quot;),
    line = element_line(size = 1),
    text = element_text(family = &quot;IBM Plex Sans&quot;),
    aspect.ratio = 2.76/1
  )</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/modify_major_elements-1.svg" width="672" /></p>
<p>With this, we can see each of the major elements modified. The rectangle fill component is now light blue (#5AF), but we can see many rectangles did not inherit the change. Preventing universal fill inheritance is presumably a sanity check by the developers, since globally changing all rectangular fills is not something most people want to do.</p>
<p>The lines are now set at a thickness of 1, so we can see the plot grid and tick lines are much larger. Notably, the box plots themselves are not modified by theme changes. The text is now set to IBM Plex Sans, which I use throughout my blog. The aspect ratio is set to a cinematic 2.76:1, which would be an appropriate ratio for an IMAX documentary.</p>
</div>
<div id="modifying-minor-elements" class="section level2">
<h2>Modifying minor elements</h2>
<p>Now let’s demonstrate modifying some minor elements. We’ll keep the global text element change, but the other changes were not practical settings and were really only useful for demonstration.</p>
<pre class="r"><code>data %&gt;%
  ggplot() +
  geom_boxplot(
    mapping = aes(
      x = Reservoir,
      y = Release,
      fill = Reservoir)
    ) + 
  facet_wrap(
    Site ~ .,
    scales = &quot;free_y&quot;) +
  labs(
    title = &quot;Water Release from Upstate Reservoirs (millions of gallons)&quot;,
    caption = &quot;Source: NYC Department of Environmental Protection&quot;
  ) +
  theme(
    text = element_text(family = &quot;IBM Plex Sans&quot;),
    axis.ticks.x = element_blank(),
    axis.ticks.y = element_blank(),
    axis.text.x = element_text(angle = 90),
    legend.title.align = .5,
    legend.key = element_blank(),
    legend.background = element_rect(color = &quot;#EEEEEE&quot;),
    panel.background = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.major.y = element_line(color = &quot;#EEEEEE&quot;),
    plot.caption.position = &quot;plot&quot;,
    strip.background = element_blank()
  )</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/modify_minor_elements-1.svg" width="672" /></p>
<p>This now looks much more presentable. I won’t explain every individual change, but I modified at least one item from each minor element category. If it seems like there are quite a few lines defining these changes, every theme actually has several hundred elements defined, even if any given element is blank.</p>
<p>I would print out the results of a theme object here, but I don’t a good way to do that without printing a 395-line block of code. If you’re interested, enter <code>theme_gray()</code> to see details for the standard theme. I used <code>theme_test()</code> as a minimal base to reduce the lines I had to write while patchworking as it has many blank elements.</p>
<p>Speaking of blank elements, undefined (and thus unused) elements are <code>NULL</code> values for all default themes and those that come with packages like <code>ggthemes</code>. Above I used a special function called <code>element_blank()</code> to set given theme elements to <code>NULL</code>. Modifying <code>theme()</code> only modifies the current plot, not future ones.</p>
</div>
<div id="updating-and-building-themes" class="section level2">
<h2>Updating and building themes</h2>
<p>Instead of the tedious, repetitive, and error-prone task of modifying each plot with <code>theme()</code>, it’s much more efficient and practical to set a theme once. I plan to use R heavily, so implementing a personal theme makes sense for me. Some ready-made themes are close to what I want, particularly <code>theme_test()</code>, but I’d like more control.</p>
<p>To get an idea of how others have used ggplot theming, I surveyed a number of style guides written by organizations that use R for inspiration, including <a href="https://bbc.github.io/rcookbook/">the BBC’s R Cookbook</a>. <a href="https://towardsdatascience.com/5-steps-for-creating-your-own-ggplot-theme-656e79a96b9">This TDS article</a> was helpful in thinking about developing themes, and the actual <a href="https://github.com/tidyverse/ggplot2/blob/master/R/theme-defaults.r">code for the default <code>theme_grey()</code></a> was also insightful.</p>
<p>After this survey, it seems that for my immediate purposes and RMarkdown blog workflow that the most efficient route for now is to set the changes I want once at the beginning of a post and then reuse the code for each post. Eventually, I’d like to create a package for a full theme. This is <a href="https://github.com/bbc/bbplot/blob/master/R/bbc_style.R">the approach the BBC took</a>.</p>
<p>Their package has two functions, <code>bbc_style()</code> and <code>finalise_plot()</code>, which adds branding and proper spacing and such. Their typical plot could look something like <code>data %&gt;% ggplot() + bbc_style() + finalise_plot()</code> with relevant geometries and <code>theme()</code> tweaks added in between.</p>
<p>Instead of adding the theme to each plot, I’d add these lines to my setup code chunk:</p>
<pre><code>library(tidyverse)
# install.packages(&#39;devtools&#39;)
# devtools::install_github(&#39;aemacleod/theme_aem&#39;)
library(theme_aem)
theme_set(theme_aem)</code></pre>
<p>That would make the theme apply by default so that <code>+ theme_aem()</code> wouldn’t be necessary for each plot, only per-plot modifications with <code>theme()</code>. For now, I will add the changes I want to make to my initial setup code chunk and then reuse that code in subsequent RMarkdown post documents.</p>
</div>
<div id="creating-a-personal-theme" class="section level2">
<h2>Creating a personal theme</h2>
<p>The <a href="https://ggplot2.tidyverse.org/reference/theme_get.html">ggplot2 documentation</a> describes some of the basics of writing a theme: <code>theme_get()</code> will return the current theme, by default <code>theme_gray()</code>. <code>theme_set(theme_test())</code> changes the theme to <code>theme_test()</code> for future plots, <code>theme_update()</code> updates elements but does not delete unspecified ones, and <code>theme_replace()</code> deletes unspecified theme elements.</p>
<p>To demonstrate the basics of this, I have taken the theme modifications I used above but put them in the function <code>theme_update()</code> instead of <code>theme()</code>. They should now become the default settings for subsequent plots.</p>
<pre class="r"><code>  theme_update(
    text = element_text(family = &quot;IBM Plex Sans&quot;),
    axis.ticks.x = element_blank(),
    axis.ticks.y = element_blank(),
    axis.text.x = element_text(angle = 90),
    legend.title.align = .5,
    legend.key = element_blank(),
    legend.background = element_rect(color = &quot;#EEEEEE&quot;),
    panel.background = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.major.y = element_line(color = &quot;#EEEEEE&quot;),
    plot.caption.position = &quot;plot&quot;,
    strip.background = element_blank()
  )</code></pre>
<p>Now I will repeat the same code block I used at the top of this post to confirm that the changes to the working theme were saved:</p>
<pre class="r"><code>data %&gt;%
  ggplot() +
  geom_boxplot(
    mapping = aes(
      x = Reservoir,
      y = Release,
      fill = Reservoir)
    ) + 
  facet_wrap(
    Site ~ .,
    scales = &quot;free_y&quot;) +
  labs(
    title = &quot;Water Release from Upstate Reservoirs (millions of gallons)&quot;,
    caption = &quot;Source: NYC Department of Environmental Protection&quot;
  )</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/demonstrate_theme_update-1.svg" width="672" /></p>
<p>These are not all the changes I would make to the theme, but they are a good representative sample. The full theme I drafted to address all theme elements has 366 lines of code, so it’s more than I want to put directly into any document. I saved it in a separate file, <code>theme_aem.r</code> and will need to import it in order to use it.</p>
<p>The R Markdown Cookbook includes <a href="https://bookdown.org/yihui/rmarkdown-cookbook/source-script.html">instructions for importing scripts</a> from external sources. First, I’ll set the theme back to the default with <code>theme_set()</code>, then import my theme document and plot the data again. I added the x axis text rotation manually with <code>theme()</code> because that’s a non-standard modification that I did not include in my theme.</p>
<pre class="r"><code>theme_set(theme_gray())
source(&quot;~/Documents/theme_aem.R&quot;, local = knitr::knit_global())
data %&gt;%
  ggplot() +
  geom_boxplot(
    mapping = aes(
      x = Reservoir,
      y = Release,
      fill = Reservoir)
    ) + 
  facet_wrap(
    Site ~ .,
    scales = &quot;free_y&quot;) +
  labs(
    title = &quot;Water Release from Upstate Reservoirs (millions of gallons)&quot;,
    caption = &quot;Source: NYC Department of Environmental Protection&quot;
  ) +
  theme(
    axis.text.x = element_text(angle = 90)
  )</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/import_theme-1.svg" width="672" /></p>
<p>These theme settings work for this particular plot, which has many common elements and was good for demonstration, but I will need to battle test it over time with other plots to make sure all the individual settings make sense. I will also experiment with different variations to see if there are minor variations I like better.</p>
<p>It’s a decent start, and it will let me save a lot of time and effort manually adjusting plots.</p>
</div>
