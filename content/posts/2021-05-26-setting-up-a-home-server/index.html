---
title: Setting up a home server
date: '2021-05-26'
slug: setting-up-a-home-server
---

<script src="{{< blogdown/postref >}}index_files/header-attrs/header-attrs.js"></script>


<p>This is a post to describe the setup of a Raspberry Pi running Alpine Linux. I will install PostgreSQL later to use the system as a practical way to run “real” SQL queries on a live database and build the practical database manipulation and data wrangling skills that are critical for a good analyst.</p>
<p>Skill in data manipulation, analysis, and visualization are also the foundation of more complex work including statistical analysis, data engineering, geographic information systems (GIS), and data science.</p>
<p>The combination of simple projects and regular posts will also act as a professional communication exercise and work portfolio demonstrating domain proficiency, writing ability, and thought processes.</p>
<div id="why-this-project" class="section level2">
<h2>Why this project</h2>
<p>For a few years now, I have been following academic research across a range of environmental fields, and I am familiar with many examples of how modern data tools and techniques are influencing other research areas.</p>
<p>This nexus of data, environmental work, and research commercialization fascinates me and so at the beginning of this year I decided that I want to specialize in data work related to the environment.</p>
<p>While the work of researchers is important and related to the larger work I want to do, my experience in higher education administration convinced me that I do not want to be a researcher myself.</p>
<p>I believe that supporting this work as a data professional, either as a freelancer, consultant, teacher, or in a relevant corporate setting, will be more enjoyable and sustainable for me and likely more impactful.</p>
<p>Since the areas that I want to focus on in the near future (data analysis, data visualization, statistical applications, and machine learning) are much more applied in nature, portfolios are a common way to demonstrate competence, communication ability, and showcase work projects.</p>
<p>I have started exploring open datasets available from various sources and believe that these are a good starting point for building projects. I will approach these datasets with several questions in mind:</p>
<ul>
<li>How can I use this to demonstrate technical ability?</li>
<li>What questions can this dataset answer, and why are they important?</li>
<li>What stories are in this data and how can I tell them best?</li>
<li>How can I create something out of this that is beautiful, compelling, and entertaining?</li>
</ul>
<p>In undertaking work like this, it is important to find a balance between big picture strategic concerns and the very nitty gritty details. It is also important to strike a balance between exploration and focus: by habit I explore more than I focus, and so creating this blog is also an effort to push myself to focus on concrete products.</p>
</div>
<div id="into-the-nitty-gritty" class="section level2">
<h2>Into the nitty gritty</h2>
<p>I will write more about how I am “project managing” myself and my efforts, but I have been focusing first on understanding the larger landscape of contemporary data work. Tool-wise, I have been studying the R programming language for analysis and discovering the ecosystem that exists around it, including the <code>blogdown</code> package I am using to create this blog.</p>
<p>Competence in using R is important, but the training exercises containing well-structured and well-behaved data are not representative of the real world. Most data worth studying has to be dragged into the analysis kicking and screaming.</p>
<p>To that end, I have started brushing up on my limited SQL skills and will need a “real” database to run queries against. Eventually, I may be able to use it for web crawling, ingesting data, and other more intensive work, but for now I will consider the project successful if I can connect to the database, import existing CSV-formatted datasets into tables, and successfully run queries against them.</p>
</div>
<div id="hardware-and-software-choices" class="section level2">
<h2>Hardware and software choices</h2>
<p>In order to run learning projects involving SQL, I am going to set up a basic home server on a Raspberry Pi. The Pi has lots of quirks and drawbacks, but it is cheap, I have it on hand, and it is powerful enough for the tasks at hand.</p>
<p>In addition to studying SQL, I am also interested in (eventually) learning more about containerization and cloud deployment using technologies like Docker, so I will use Alpine Linux, a distribution designed to be small and secure which also runs from RAM, making it faster. Plus, I like trying new distros.</p>
<p>There are many well-regarded enterprise grade database programs and I do not have enough experience with database administration in a business context to have a strong preference for any of them. I am picking PostgreSQL for the database for the following reasons, in no particular order:</p>
<ul>
<li>It is widespread and proven technology</li>
<li>It is widely taught, making it easier to find courses, guides, and forum posts for specific issues</li>
<li>It is free and open source software (FOSS)</li>
<li>Its SQL dialect is fairly standard</li>
<li>It has good documentation</li>
<li>It has grown in popularity in recent developer surveys and job posting estimates</li>
</ul>
</div>
<div id="install-alpine-to-sd" class="section level2">
<h2>Install Alpine to SD</h2>
<p>(If you are not interested in reading highly specific technical instructions, feel free to stop reading here.)</p>
<p>The base system of the database will be Alpine Linux running on a Raspberry Pi. Since I will be connecting remotely, there will be no need for a desktop environment and I will run the server headless (no monitor or keyboard).</p>
<p>I used <a href="https://wiki.alpinelinux.org/wiki/Raspberry_Pi">this guide</a> for preparation of the SD card and this <a href="https://wiki.alpinelinux.org/wiki/Raspberry_Pi_-_Headless_Installation">headless installation guide</a> from the Alpine wiki.</p>
<ol style="list-style-type: decimal">
<li>Get the tarball from <a href="https://alpinelinux.org/downloads/">Alpine</a>. Since we are using a more recent Raspberry Pi 4, download the <code>aarch64</code> build (<code>alpine-rpi-3.13.5-aarch64.tar.gz</code> as of writing).</li>
<li>Open the GNOME Disks utility (package <code>gnome-disks</code>, but <code>dd</code> can also be used), click the triple dot icon, and select ‘Format Disk.’</li>
<li>In the ‘Erase’ dropdown, select either Quick or Slow, it makes no difference for our purposes here. For the ‘Partitioning’ dropdown, select the ‘GPT’ option.</li>
<li>Click the + icon to create a partition in unallocated space. I am using a 32gb SD card and allocated all available space to the partition. The volume label can be anything, <code>ALPINE</code> is fine. The type of partition is ‘FAT’. Once the new partition is created, it may be automatically mounted. You should see a ⏵ icon below the Volumes diagram. If you see a square icon, click it to unmount the SD card.</li>
<li>Keep Disks open and switch to an archiving utility to extract the tarball onto the SD card’s root directory. I used another GNOME utility, Archive Manager (<code>file-roller</code>), to do this. Be careful not to extract the <code>.</code> directory itself to the SD card, instead extract the <em>contents</em> of the directory.</li>
<li>While the drive is still unmounted, open a terminal and run the command <code>lsblk</code> to list your devices. SD cards will usually be <code>sdX</code> where is a number, in my case the it is <code>mmcblk0</code> and the partition is <code>mmcblk0p1</code>.</li>
<li>Run the command <code>fatlabel /dev/mmcblk0p1 ALPINE</code> to change the volume name, changing <code>mmcblk0p1</code> for whatever value your partition is assigned. This is necessary because of a <a href="https://github.com/raspberrypi/firmware/issues/1529">current bug</a> in the RPi firmware. The solution came from <a href="https://gitlab.alpinelinux.org/alpine/aports/-/issues/12368">this issue</a>. If you do not change the volume name or if you do not unmount the volume before running <code>fatlabel</code>, then your Pi will not boot and the red indicator light will flash solid.</li>
<li>Back in Disks, click the ⏵ icon to mount the drive. We are done with the Disks program now.</li>
</ol>
<p>At this point, you could hook the Pi up to a keyboard, monitor, and ethernet jack and it should boot properly. To run the server headless without additional setup from the local command line, we need to do some extra work.</p>
</div>
<div id="create-headless-overlay" class="section level2">
<h2>Create headless overlay</h2>
<ol style="list-style-type: decimal">
<li>On your local computer (not on the SD card), type <code>mkdir etc</code> from the terminal to create a directory.</li>
<li>Inside this directory, type <code>touch .default_boot_services</code> and then <code>mkdir local.d runlevels</code> to create the next level of files. Type <code>ls -a</code> to confirm that all 3 files were created.</li>
<li>Type <code>cd runlevels</code> to navigate to that directory, and type <code>mkdir default</code> to create a new directory. Type <code>cd default</code> to navigate there. In this directory, create a symlink by typing <code>ln -s /etc/init.d/local local</code>.</li>
<li>Move to <code>etc/local.d</code> by typing <code>cd ../../local.d</code> and then <code>touch headless.start</code> to create a script file.</li>
<li>Type <code>nano headless.start</code> to open the file, copy the following script, and type <code>Ctrl+o</code> and <code>Enter</code> to save, then <code>Ctrl+x</code> to exit. Type <code>cd ..</code> to return to <code>etc</code>.</li>
</ol>
<pre><code>#!/bin/sh

__create_eni()
{
    cat &lt;&lt;-EOF &gt; /etc/network/interfaces
    auto lo
    iface lo inet loopback

    auto ${iface}
    iface ${iface} inet dhcp
            hostname localhost
    EOF
}

__create_eww()
{
    cat &lt;&lt;-EOF &gt; /etc/wpa_supplicant/wpa_supplicant.conf
    network={
            ssid=&quot;${ssid}&quot;
            psk=&quot;${psk}&quot;
    }
    EOF
}

__edit_ess()
{
    cat &lt;&lt;-EOF &gt;&gt; /etc/ssh/sshd_config
    PermitEmptyPasswords yes
    PermitRootLogin yes
    EOF
}

__find_wint()
{
    for dev in /sys/class/net/*
    do
        if [ -e &quot;${dev}&quot;/wireless -o -e &quot;${dev}&quot;/phy80211 ]
        then
            echo &quot;${dev##*/}&quot;
        fi
    done
}

ovlpath=$(find /media -name *.apkovl.tar.gz -exec dirname {} \;)
read ssid psk &lt; &quot;${ovlpath}/wifi.txt&quot;

if [ ${ssid} ]
then
  iface=$(__find_wint)
  apk add wpa_supplicant
  __create_eww
  rc-service wpa_supplicant start
else
  iface=&quot;eth0&quot;
fi

__create_eni
rc-service networking start

/sbin/setup-sshd -c openssh
cp /etc/ssh/sshd_config /etc/ssh/sshd_config.orig
__edit_ess
rc-service sshd restart
mv /etc/ssh/sshd_config.orig /etc/ssh/sshd_config</code></pre>
<p>Now your directory tree is complete, and typing <code>tree -a</code> should give the following output:</p>
<pre><code>penguin@localhost:~/etc$ tree -a
.
├── .default_boot_services
├── local.d
│   └── headless.start
└── runlevels
    └── default
        └── local -&gt; /etc/init.d/local</code></pre>
<ol style="list-style-type: decimal">
<li>From the working directory (where we created <code>etc</code>, but not inside <code>etc</code>), create a zip by running <code>tar czvf headless.apkovl.tar.gz etc/</code> and then copy the resulting file to the root directory of the SD card. <code>apkovl</code> files are overlay files used by Alpine to persist data between reboots, you can read more about how they work <a href="https://wiki.alpinelinux.org/wiki/Alpine_local_backup#Basic_.22commit.22_command">here</a>.</li>
<li>Even though I am not using wifi, the above script will fail if it does not find a file called <code>wifi.txt</code> in the root directory, so create an empty file with <code>touch wifi.txt</code> and copy it to the SD card’s root directory.</li>
</ol>
<p>Now we are ready to log into the system.</p>
</div>
<div id="logging-in-and-basic-setup" class="section level2">
<h2>Logging in and basic setup</h2>
<ol style="list-style-type: decimal">
<li>Unmount the SD card and eject it, insert it into the Pi. Connect the etnernet and power on the device.</li>
<li>After a few minutes, check your router for new DHCP leases. The process for doing this varies by vendor but the number will generally be have the format <code>192.168.###.###</code>. If you are connected to a VPN, disconnect and attempt to log in by typing <code>ssh root@192.168.###.###</code>.</li>
<li>You will be asked to accept a fingerprint, type <code>yes</code> and you should then be logged in.</li>
<li>Instead of running the standard <code>setup-alpine</code> collection of scripts to set up the system, we will avoid <code>setup-sshd</code> and <code>setup-interfaces</code> by manually running the other setup scripts:
<ul>
<li>setup-ntp</li>
<li>setup-keymap</li>
<li>setup-hostname</li>
<li>setup-timezone</li>
<li>setup-apkrepos</li>
<li>setup-lbu</li>
<li>setup-apkcache</li>
</ul></li>
<li>In my case, the <code>chronyd</code> timekeeping service does not start automatically after running <code>setup-ntp</code>, which can be confirmed by typing <code>date</code>. As a result of the time being wrong, I get an error when I run <code>setup-apkrepos</code>. I correct this by running <code>rc-service chronyd restart</code>. <code>date</code> now shows the correct time and <code>setup-apkrepos</code> runs correctly.</li>
<li>We will commit changes by typing <code>lbu commit -d</code> so that they persist between reboots, and then type <code>reboot</code> to restart the system and confirm that our changes persisted.</li>
</ol>
<p>At this point, we have a basic installation running. Alpine does very little by default, so many things that we will want to do are not set up. The <a href="https://wiki.alpinelinux.org/wiki/Installation#Post-Install">Post-Install section</a> of the Installation page on the Alpine wiki is a good place to start. In the next post, we will set up PostgreSQL.</p>
</div>
