---
title: "Connecting RPi to pgAdmin4 and RStudio"
date: '2021-06-01'
slug: 2021-06-01-connecting-rpi-to-pgAdmin4-and-RStudio
---

<script src="{{< blogdown/postref >}}index_files/header-attrs/header-attrs.js"></script>


<p>Fortunately, the basic settings we input when <a href="https://aem.run/posts/2021-05-30-installing-postgresql-on-alpine-rpi/">installing postgres</a> worked without issue in pgAdmin4. Troubleshooting configuration files, sockets, ports, identity files, and the other minutiae of modern network security is a very easy way to burn a lot of time.</p>
<div id="connecting-to-pgadmin4" class="section level2">
<h2>Connecting to pgAdmin4</h2>
<ul>
<li><code>ssh</code> into the Pi from a terminal and ensure that <code>PermitTunnel = yes</code> in the file <code>/etc/ssh/sshd_config</code></li>
<li>Install and open pgAdmin4, then create a server connection:</li>
</ul>
<p><img src="images/create%20server.png" style="width:60.0%" /></p>
<ul>
<li>The server name is a label for inside pgAdmin4, so we can set it to just about anything. Set the colors if you so choose, but they don’t play very well with dark themes.</li>
</ul>
<p><img src="images/general.png" style="width:60.0%" /></p>
<ul>
<li>We’re connecting through <code>ssh</code>, so we’ll be using the port we set in <code>/var/lib/postgresql/data/postgresql.conf</code>, which is 5432 by default. <code>localhost</code> is the default hostname in Alpine’s <code>setup-hostname</code> script, and <code>postgres</code> is the default database username we created when setting up postgres. There are options to enter and save the password when we first set up the connection.</li>
</ul>
<p><img src="images/connection.png" style="width:60.0%" /></p>
<ul>
<li>We’re not using SSL for our connection, so this tab is blank.</li>
</ul>
<p><img src="images/ssl.png" style="width:60.0%" /></p>
<ul>
<li>We are connecting via <code>ssh</code>, so choose Yes. The tunnel host is the Pi’s local IP address, which is a temporary address (called a DHCP lease) assigned by the router that usually starts with 192.168. The port, 22 by default, is set in <code>/etc/ssh/sshd_config</code> on Alpine. The user name is postgres. There are options to enter and save the password when we first set up the connection.</li>
</ul>
<p><img src="images/ssh-tunnel.png" style="width:60.0%" /></p>
<ul>
<li>We don’t have any advanced settings to change, the 10 second setting auto connection attempt timeout is a sensible default so we’ll leave it.</li>
</ul>
<p><img src="images/advanced.png" style="width:60.0%" /></p>
<ul>
<li>And that’s it. Once we click Save, we enter the password for <code>postgres</code> and our connection is live.</li>
</ul>
<p><img src="images/success.png" style="width:60.0%" /></p>
</div>
<div id="connecting-to-rstudio" class="section level2">
<h2>Connecting to RStudio</h2>
<p>We’ll be using <a href="https://db.rstudio.com/">the RStudio database guide</a> and <a href="https://www.r-bloggers.com/2018/07/connecting-r-to-postgresql-on-linux/">this blog post</a> as a reference and for additional context.</p>
<ol style="list-style-type: decimal">
<li>Set a password for the postgres database on our RPi:
<ul>
<li>Open a terminal</li>
<li><code>ssh postgres@192.168.xxx.xxx</code></li>
<li><code>psql</code></li>
<li><code>ALTER USER postgres PASSWORD 'my_new_secure_password';</code></li>
<li><code>\q</code></li>
</ul></li>
<li>On the local system, install some packages that are necessary for RStudio packages that we will install:<br />
<code>sudo apt install libpq-dev unixodbc unixodbc-dev odbc-postgresql</code></li>
<li>From the RStudio console, install the relevant packages and confirm the needed drivers are available:
<ul>
<li><code>install.packages("DBI")</code></li>
<li><code>install.packages("odbc")</code></li>
<li><code>install.packages("RPostgres")</code></li>
<li><code>library(odbc)</code></li>
<li><code>sort(unique(odbcListDrivers()[[1]]))</code><br />
</li>
</ul></li>
<li>Create the connection. I recommend saving the command in an R script. If you use <code>odbc</code>, RStudio saves the details in a dedicated Connections tab, but it’s also good to have it available just in case.</li>
</ol>
<pre><code>library(DBI)
library(odbc)
library(RPostgres)

# Using ODBC
con &lt;- DBI::dbConnect(odbc::odbc(),
                             driver = &quot;PostgreSQL Unicode&quot;,
                             server = &quot;192.168.xxx.xxx&quot;,
                             host = &quot;localhost&quot;,
                             database = &quot;postgres&quot;,
                             UID = rstudioapi::askForPassword(&quot;Input user name&quot;),
                             PWD = rstudioapi::askForPassword(&quot;Input password&quot;), 
                             port = 5432)

# Using RPostgres package
con &lt;- DBI::dbConnect(RPostgres::Postgres(),
                      dbname = &quot;postgres&quot;,
                      host = &quot;192.168.xxx.xxx&quot;,
                      port = 5432,
                      user = rstudioapi::askForPassword(&quot;Input database user name&quot;),
                      password = rstudioapi::askForPassword(&quot;Input database user password&quot;)
                      )

# Disconnect when finished
DBI::dbDisconnect()
# Clear environment
rm(list = ls())
# Clear packages
pacman::p_unload(all)</code></pre>
</div>
<div id="how-not-to-run-queries" class="section level2">
<h2>How <em>not</em> to run queries</h2>
<p>The database guide lists three methods for running SQL queries against a database from within RStudio: DBI, dplyr/dbplyr, and raw SQL within R Notebooks.</p>
<p>As I am interested in working with SQL more directly rather than converting R code with <code>dplyr</code>/<code>dbplyr</code>, it seemed like it would make sense to do the following within an R Markdown document:</p>
<ol style="list-style-type: decimal">
<li>Write the database connection in one code chunk</li>
<li>Run the SQL code, referencing the connection, as a separate chunk</li>
<li>Do the analysis and manipulation</li>
</ol>
<p>After some experimentation, I am convinced that this was a bad idea. In addition to the actual analysis, I am using this Rmd document with the <code>blogdown</code> package (which runs on top of the <code>hugo</code> static site generator to create a blog post), and funny things happened when I embedded a database connection in the source document.</p>
<p>Specifically, RStudio crashed a number of times, and even before that it seemed quite unhappy with what I was asking it to do. It appeared to be running an infinite loop, and when I tried to stop the code execution, the session became unresponsive.</p>
<p>Even after restarting RStudio and deleting the offending code chunks from my Rmd document, it would not knit properly or let me preview my changes until I deleted the previously-generated index.html file.</p>
<p>I’m not sure exactly what was wrong, but if I had to hazard a guess, I suspect that <code>odbc</code>/<code>RPostgres</code> was trying to keep the connection alive while <code>blogdown</code> was trying to finish the job, so gears started grinding.</p>
<p>Clearly, this is not the right way. While I was breaking things, I did manage to run a test query successfully:</p>
<pre><code>SELECT datname FROM pg_database;</code></pre>
<p>For the time being, I believe I have gotten a little ahead of myself, so I’m going to call it a day and spend some time studying the nexus between databases and RStudio. I did manage to get a basic connection working and successfully queried my database twice, so I lumberingly met my goal.</p>
</div>
