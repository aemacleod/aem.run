---
title: 'Project: the Rube Goldberg table'
date: '2021-06-13'
slug: 2021-06-13-project-the-rube-goldberg-table
output:
  html_document:
    df_print: kable
---

One of the reasons I started this blog was to be act as a portfolio and example of analytical products. As I've started writing, I also realize there are other types of posts that I like to make, including technical documentation and open workbooks for experimentation.

Posts like this are less interesting to general, non-technical audiences and serve as a halfway point between the accumulated notes, files, bookmarks, and code samples stored locally for personal reference, and the more polished products designed for wide interest and impact.

There are lots of ways to run a personal blog that can exist in tension with each other. In a competition between staying on message/on brand and being a nerd doing deep dives, the deep dives win for me, at least for the work I want to do in the near term. However, I still want to have something to show less technical audiences.

I've thought about how I can write for multiple registers (and why and when I'd want to address each), and while I'm always going to skew nerdy, I come back to a recommendation for analysts I've seen repeated in a number of places: focus on explanation rather than exploration.

There are process nerds out there, but writing only for specialists is insular and bad practice long term. When addressing non-specialists, and the ability to address general audiences *should* be cultivated, the focus is on what we can extract from the data rather than the extraction process. For technical writing, that means I have three registers:

- Technical posts for "thinking out loud" and going through the very fine details of process
- Project reports explaining how these regular posts cohere and build towards larger goals
- Analytical products that address the implicit question "why should I care?"

Since I have lately wrapped up my first project, this will be my first writeup.

## The Rube Goldberg table

Rube Goldberg was a cartoonist who became famous in the early 20th century for his drawings of complex and intricate machines which achieved very basic results through overwrought processes. By following a comparably long and circuitous but admittedly less visually stimulating route, I have create this table:

```{r setup, include=FALSE}
library(tidyverse)
library(kableExtra)
knitr::opts_chunk$set(
  echo = FALSE,
  message = FALSE,
  warning = FALSE
  )
```

```{r table}
data <- read_csv("table.csv")
data <- data %>%
  mutate(Dataset = str_c("[",data$dataset_title,"]",
                         "(",data$url,")")
         ) %>%
  select(Dataset, Notes = notes,
         Columns = num_columns,
         Rows = num_rows,
         -dataset_title, -url
         )
data %>%
  kbl(align='llcc',
      caption='NYC Environmental Open Data Sets of Interest') %>%
  kable_styling(font_size=12) %>%
  column_spec(1, width_min="15em",
              new_tab=T) %>%
  column_spec(2, width_min="30em")
```

These are data sets that I plan to use in the coming weeks for progressively building my data skills in R. It would not have been difficult to write this table in Markdown, but the point of this learning project was to plan and build a database workflow from scratch.

This post is to briefly summarize each step and talk about the related skills I built, improved on, or demonstrated along the way. These are the steps I took:

1. Setting up the physical server
1. Initializing the database and preparing it for remote connections
1. Connecting to the database from RStudio and pgAdmin4, a dedicated program for managing PostgreSQL
1. Running SQL queries within RStudio to extract data from the database and save it locally
1. Processing and presenting the extracted data

## Setting up

Skills: research, project planning, technical writing, Linux (command line utilities, managing services), networking, secure shell (ssh).

[Here](https://aem.run/posts/setting-up-a-home-server) I established the reason, goals, scope, and success conditions for the project and wrote out instructions for setting up a basic headless (lacking a monitor or keyboard, only remotely connecting) server running Alpine Linux on a Raspberry Pi.

## Initializing the database

Skills: Linux (system administration, user and role management, shell scripting, research and error resolution)

[Here](https://aem.run/posts/2021-05-30-installing-postgresql-on-alpine-rpi/) I installed and set up PostgreSQL on the server, troubleshooting some of the challenges that come with using a less-common system setup. Specifically, Alpine Linux runs from RAM and does not automatically save changes between reboots, so I used system utilities and shell scripting knowledge to ensure that the system works as expected.

## Connecting to the database

Skills: research, technical writing

[Here](https://aem.run/posts/2021-06-01-connecting-rpi-to-pgadmin4-and-rstudio/) I established connections with the database from pgAdmin4, the PostgreSQL graphical application, and RStudio.

## Running SQL queries

Skills: SQL, PostgreSQL, R, process design

[Here](https://aem.run/posts/2021-06-06-running-sql-queries-in-rstudio/) I built a workflow for interacting with a postgres database from within RStudio and then implemented it with sample data, which I queried and then saved to the local disk for processing.

## Processing and presenting

Skills: data wrangling, research, visualization, CSS/HTML

[Here](https://aem.run/posts/2021-06-10-beautifying-a-table/) I used the previously imported data to create the table seen above, adjusting the columns and using packages from the R ecosystem to make it more attractive and integrate with the native styling of my blog's theme.

## Conclusion

SQL is a foundational skill for contemporary data work because so much of the world's data is in SQL databases. By building this server, I now have a safe environment where I can experiment without risking important data. I'll also be able to build, demonstrate, use, and eventually teach the skills I develop, and incorporate them practically in my workflows.

I may also be able to put the database (and the Pi more generally) to work on more advanced workflows like web scraping once I learn the basics of those skills.