---
title: 'Project: the Rube Goldberg table'
date: '2021-06-13'
slug: 2021-06-13-project-the-rube-goldberg-table
output:
  html_document:
    df_print: kable
---

<script src="{{< blogdown/postref >}}index_files/header-attrs/header-attrs.js"></script>
<script src="{{< blogdown/postref >}}index_files/kePrint/kePrint.js"></script>
<link href="{{< blogdown/postref >}}index_files/lightable/lightable.css" rel="stylesheet" />


<p>One of the reasons I started this blog was to be act as a portfolio and example of analytical products. As I’ve started writing, I also realize there are other types of posts that I like to make, including technical documentation and open workbooks for experimentation.</p>
<p>Posts like this are less interesting to general, non-technical audiences and serve as a halfway point between the accumulated notes, files, bookmarks, and code samples stored locally for personal reference, and the more polished products designed for wide interest and impact.</p>
<p>There are lots of ways to run a personal blog that can exist in tension with each other. In a competition between staying on message/on brand and being a nerd doing deep dives, the deep dives win for me, at least for the work I want to do in the near term. However, I still want to have something to show less technical audiences.</p>
<p>I’ve thought about how I can write for multiple registers (and why and when I’d want to address each), and while I’m always going to skew nerdy, I come back to a recommendation for analysts I’ve seen repeated in a number of places: focus on explanation rather than exploration.</p>
<p>There are process nerds out there, but writing only for specialists is insular and bad practice long term. When addressing non-specialists, and the ability to address general audiences <em>should</em> be cultivated, the focus is on what we can extract from the data rather than the extraction process. For technical writing, that means I have three registers:</p>
<ul>
<li>Technical posts for “thinking out loud” and going through the very fine details of process</li>
<li>Project reports explaining how these regular posts cohere and build towards larger goals</li>
<li>Analytical products that address the implicit question “why should I care?”</li>
</ul>
<p>Since I have lately wrapped up my first project, this will be my first writeup.</p>
<div id="the-rube-goldberg-table" class="section level2">
<h2>The Rube Goldberg table</h2>
<p>Rube Goldberg was a cartoonist who became famous in the early 20th century for his drawings of complex and intricate machines which achieved very basic results through overwrought processes. By following a comparably long and circuitous but admittedly less visually stimulating route, I have create this table:</p>
<table class="table" style="font-size: 12px; margin-left: auto; margin-right: auto;">
<caption style="font-size: initial !important;">
<span id="tab:table">Table 1: </span>NYC Environmental Open Data Sets of Interest
</caption>
<thead>
<tr>
<th style="text-align:left;">
Dataset
</th>
<th style="text-align:left;">
Notes
</th>
<th style="text-align:center;">
Columns
</th>
<th style="text-align:center;">
Rows
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;min-width: 15em; ">
<a href="https://data.cityofnewyork.us/Environment/Drinking-Water-Quality-Distribution-Monitoring-Dat/bkwf-xfky">Drinking Water Quality Distribution Monitoring Data</a>
</td>
<td style="text-align:left;min-width: 30em; ">
Many sites (~400), fewer variables, many observations, would be good set for data wrangling
</td>
<td style="text-align:center;">
11
</td>
<td style="text-align:center;">
95400
</td>
</tr>
<tr>
<td style="text-align:left;min-width: 15em; ">
<a href="https://data.cityofnewyork.us/Environment/Distribution-Sites-LCR-Monitoring-Results/g63j-swsd">Distribution Sites LCR Monitoring Results</a>
</td>
<td style="text-align:left;min-width: 30em; ">
Many testing sites, compelling data (lead and copper at the tap), relatively simple set would be good for more impactful analysis (e.g. statistical approaches, storytelling)
</td>
<td style="text-align:center;">
10
</td>
<td style="text-align:center;">
1411
</td>
</tr>
<tr>
<td style="text-align:left;min-width: 15em; ">
<a href="https://data.cityofnewyork.us/Environment/Entry-Point-LCR-Monitoring-Results/aa5e-digs">Entry Point LCR Monitoring Results</a>
</td>
<td style="text-align:left;min-width: 30em; ">
Testing sites are rows, would be good tidy data presentation, is a fairly simple data set
</td>
<td style="text-align:center;">
10
</td>
<td style="text-align:center;">
8181
</td>
</tr>
<tr>
<td style="text-align:left;min-width: 15em; ">
<a href="https://data.cityofnewyork.us/Environment/Watershed-Water-Quality-Data/y43c-5n92">Watershed Water Quality Data</a>
</td>
<td style="text-align:left;min-width: 30em; ">
Simple data set conceptually and structurally, could be used for more substantial analyses, e.g. stats, multivariate, regressions
</td>
<td style="text-align:center;">
10
</td>
<td style="text-align:center;">
2431
</td>
</tr>
<tr>
<td style="text-align:left;min-width: 15em; ">
<a href="https://data.cityofnewyork.us/Environment/DEP-Cryptosporidium-And-Giardia-Data-Set/x2s6-6d2j">DEP - Cryptosporidium And Giardia Data Set</a>
</td>
<td style="text-align:left;min-width: 30em; ">
Sample sites are listed in rows rather than as column variables, this would be good tidy data presentation. Data overlaps with Watershed Water Quality Data, tables could be pulled together for more complex SQL analysis or data table manipulations
</td>
<td style="text-align:center;">
5
</td>
<td style="text-align:center;">
1065
</td>
</tr>
<tr>
<td style="text-align:left;min-width: 15em; ">
<a href="https://data.cityofnewyork.us/Environment/Compliance-at-the-tap-Lead-and-Copper-Data/3wxk-qa8q">Compliance at-the-tap Lead and Copper Data</a>
</td>
<td style="text-align:left;min-width: 30em; ">
Compelling data set, has potential interest or applicability for municipal nerds and civil groups
</td>
<td style="text-align:center;">
7
</td>
<td style="text-align:center;">
2871
</td>
</tr>
<tr>
<td style="text-align:left;min-width: 15em; ">
<a href="https://data.cityofnewyork.us/Environment/Harbor-Water-Quality/5uug-f49n">Harbor Water Quality</a>
</td>
<td style="text-align:left;min-width: 30em; ">
Many sites (40+), rows, and observation, would be good exercise for data wrangling and visualization
</td>
<td style="text-align:center;">
100
</td>
<td style="text-align:center;">
92000
</td>
</tr>
<tr>
<td style="text-align:left;min-width: 15em; ">
<a href="https://data.cityofnewyork.us/Environment/Current-Reservoir-Levels/zkky-n5j3">Current Reservoir Levels</a>
</td>
<td style="text-align:left;min-width: 30em; ">
Rows are reservoirs, columns are measurements, would make a good tidy data presentation
</td>
<td style="text-align:center;">
25
</td>
<td style="text-align:center;">
1278
</td>
</tr>
</tbody>
</table>
<p>These are data sets that I plan to use in the coming weeks for progressively building my data skills in R. It would not have been difficult to write this table in Markdown, but the point of this learning project was to plan and build a database workflow from scratch.</p>
<p>This post is to briefly summarize each step and talk about the related skills I built, improved on, or demonstrated along the way. These are the steps I took:</p>
<ol style="list-style-type: decimal">
<li>Setting up the physical server</li>
<li>Initializing the database and preparing it for remote connections</li>
<li>Connecting to the database from RStudio and pgAdmin4, a dedicated program for managing PostgreSQL</li>
<li>Running SQL queries within RStudio to extract data from the database and save it locally</li>
<li>Processing and presenting the extracted data</li>
</ol>
</div>
<div id="setting-up" class="section level2">
<h2>Setting up</h2>
<p>Skills: research, project planning, technical writing, Linux (command line utilities, managing services), networking, secure shell (ssh).</p>
<p><a href="https://aem.run/posts/setting-up-a-home-server">Here</a> I established the reason, goals, scope, and success conditions for the project and wrote out instructions for setting up a basic headless (lacking a monitor or keyboard, only remotely connecting) server running Alpine Linux on a Raspberry Pi.</p>
</div>
<div id="initializing-the-database" class="section level2">
<h2>Initializing the database</h2>
<p>Skills: Linux (system administration, user and role management, shell scripting, research and error resolution)</p>
<p><a href="https://aem.run/posts/2021-05-30-installing-postgresql-on-alpine-rpi/">Here</a> I installed and set up PostgreSQL on the server, troubleshooting some of the challenges that come with using a less-common system setup. Specifically, Alpine Linux runs from RAM and does not automatically save changes between reboots, so I used system utilities and shell scripting knowledge to ensure that the system works as expected.</p>
</div>
<div id="connecting-to-the-database" class="section level2">
<h2>Connecting to the database</h2>
<p>Skills: research, technical writing</p>
<p><a href="https://aem.run/posts/2021-06-01-connecting-rpi-to-pgadmin4-and-rstudio/">Here</a> I established connections with the database from pgAdmin4, the PostgreSQL graphical application, and RStudio.</p>
</div>
<div id="running-sql-queries" class="section level2">
<h2>Running SQL queries</h2>
<p>Skills: SQL, PostgreSQL, R, process design</p>
<p><a href="https://aem.run/posts/2021-06-06-running-sql-queries-in-rstudio/">Here</a> I built a workflow for interacting with a postgres database from within RStudio and then implemented it with sample data, which I queried and then saved to the local disk for processing.</p>
</div>
<div id="processing-and-presenting" class="section level2">
<h2>Processing and presenting</h2>
<p>Skills: data wrangling, research, visualization, CSS/HTML</p>
<p><a href="https://aem.run/posts/2021-06-10-beautifying-a-table/">Here</a> I used the previously imported data to create the table seen above, adjusting the columns and using packages from the R ecosystem to make it more attractive and integrate with the native styling of my blog’s theme.</p>
</div>
<div id="conclusion" class="section level2">
<h2>Conclusion</h2>
<p>SQL is a foundational skill for contemporary data work because so much of the world’s data is in SQL databases. By building this server, I now have a safe environment where I can experiment without risking important data. I’ll also be able to build, demonstrate, use, and eventually teach the skills I develop, and incorporate them practically in my workflows.</p>
<p>I may also be able to put the database (and the Pi more generally) to work on more advanced workflows like web scraping once I learn the basics of those skills.</p>
</div>
